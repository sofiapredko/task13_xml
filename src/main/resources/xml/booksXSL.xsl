<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">


    <xsl:template match="/">
        <html>
            <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
                <div style="background-color: green; color: black;">
                    <h2>All books</h2>
                </div>
                <table border="3">
                    <tr bgcolor="#2E9AFE">
                        <th style="width:250px">Title</th>
                        <th style="width:250px">Author</th>
                        <th style="width:350px">Type</th>
                        <th style="width:250px">Pages</th>
                        <th style="width:250px">Colourful</th>
                        <th style="width:250px">Genre</th>
                        <th style="width:250px">Readers</th>
                        <th style="width:250px">Index</th>
                    </tr>

                <xsl:for-each select="books/book">
                    <tr>
                        <td><xsl:value-of select="title"/></td>
                        <td><xsl:value-of select="author"/></td>
                        <td><xsl:value-of select="type"/></td>
                        <td><xsl:value-of select="chars/pages"/></td>
                        <td><xsl:value-of select="chars/colourful"/></td>
                        <td><xsl:value-of select="chars/genre"/></td>
                        <td><xsl:value-of select="chars/readers"/></td>
                        <td><xsl:value-of select="chars/index"/></td>
                    </tr>
                </xsl:for-each>

                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>