package com.sofia;

import com.sofia.model.Book;
import java.util.Comparator;

public class BookComparator implements Comparator<Book> {
    @Override
    public int compare(Book o1, Book o2) {
        return Integer.compare(o1.getChars().getPages(), o2.getChars().getPages());
    }
}