package com.sofia.parsers.stax;

import com.sofia.model.Book;
import com.sofia.model.Chars;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StAXReader {
    public static List<Book> parseBooks(File xml, File xsd){
        List<Book> beerList = new ArrayList<>();
        Book book = new Book();
        Chars chars = new Chars();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while(xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "title":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert book != null;
                            book.setTitle(xmlEvent.asCharacters().getData());
                            break;
                        case "author":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert book != null;
                            book.setAuthor(xmlEvent.asCharacters().getData());
                            break;
                        case "type":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert book != null;
                            book.setType(xmlEvent.asCharacters().getData());
                            break;
                        case "chars":
                            xmlEvent = xmlEventReader.nextEvent();
                            chars = new Chars();
                            break;
                        case "pages":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert chars != null;
                            chars.setPages(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "colourful":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert chars != null;
                            chars.setColourful(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        case "genre":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert chars != null;
                            chars.setGenre(xmlEvent.asCharacters().getData());
                            break;
                        case "readers":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert chars != null;
                            chars.setReaders(xmlEvent.asCharacters().getData());
                            break;
                        case "index":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert chars != null;
                            chars.setReaders(xmlEvent.asCharacters().getData());
                            break;
                    }
                }

                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();
                    if(endElement.getName().getLocalPart().equals("book")){
                        beerList.add(book);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return beerList;
    }
}
