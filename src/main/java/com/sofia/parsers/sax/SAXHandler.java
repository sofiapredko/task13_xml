package com.sofia.parsers.sax;

import com.sofia.model.Book;
import com.sofia.model.Chars;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
    private List<Book> bookList = new ArrayList<>();
    private Book book;
    private Chars chars;
    
    private boolean bTitle = false;
    private boolean bAuthor = false;
    private boolean bType = false;
    private boolean bChars = false;
    private boolean bColourful = false;
    private boolean bPages = false;
    private boolean bGenre = false;
    private boolean bReaders = false;
    private boolean bIndex = false;

    public List<Book> getBookList(){
        return this.bookList;
    }

    public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
        //if (qName.equalsIgnoreCase("book")){ book = new Book(); }
        book = new Book();
        if (qName.equalsIgnoreCase("title")){bTitle = true;}
        else if (qName.equalsIgnoreCase("author")){bAuthor = true;}
        else if (qName.equalsIgnoreCase("type")){bType = true;}
        else if (qName.equalsIgnoreCase("chars")){bChars = true;}
        else if (qName.equalsIgnoreCase("colourful")){ bColourful = true;}
        else if (qName.equalsIgnoreCase("pages")){bPages = true;}
        else if (qName.equalsIgnoreCase("genre")){bGenre = true;}
        else if (qName.equalsIgnoreCase("readers")){bReaders = true;}
        else if (qName.equalsIgnoreCase("index")){bIndex = true;}
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("book")){
            bookList.add(book);
        }
    }

    public void characters(char ch[], int start, int length) throws SAXException {
        if (bTitle){
            book.setTitle(new String(ch, start, length));
            bTitle = false;
        }
        if (bAuthor){
            book.setTitle(new String(ch, start, length));
            bTitle = false;
        }
        else if (bType){
            book.setType(new String(ch, start, length));
            bType = false;
        }
        else if (bChars){
            chars = new Chars();
            bChars = false;
        }
        else if (bColourful){
            boolean colour = Boolean.parseBoolean(new String(ch, start, length));
            chars.setColourful(colour);
            bColourful = false;
        }
        else if (bPages){
            int pages = Integer.parseInt(new String(ch, start, length));
            chars.setPages(pages);
            bPages = false;
        }
        else if (bGenre){
            String genre = new String(ch, start, length);
            chars.setGenre(genre);
            bGenre = false;
        }
        else if (bReaders){
            String readers = new String(ch, start, length);
            chars.setGenre(readers);
            bReaders = false;
        }
        else if (bIndex){
            int index = Integer.parseInt(new String(ch, start, length));
            chars.setIndex(index);
            bIndex = false;
        }
    }
}

