package com.sofia.parsers.dom;

import com.sofia.model.Book;
import com.sofia.model.Chars;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMDocReader {
    public List<Book> readDoc(Document doc) {
        doc.getDocumentElement().normalize();
        List<Book> books = new ArrayList<>();

        NodeList nodeList = doc.getElementsByTagName("book");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Book book = new Book();
            Chars chars;

            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;

                book.setTitle(element.getElementsByTagName("title").item(0).getTextContent());
                book.setAuthor(element.getElementsByTagName("author").item(0).getTextContent());
                book.setType(element.getElementsByTagName("type").item(0).getTextContent());
                chars = getChars(element.getElementsByTagName("chars"));
                book.setChars(chars);
                books.add(book);
            }
        }
        return books;
    }

    private Chars getChars(NodeList nodes) {
        Chars chars = new Chars();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            chars.setPages(Integer.parseInt(element.getElementsByTagName("pages").item(0).getTextContent()));
            chars.setColourful(Boolean.parseBoolean(element.getElementsByTagName("colourful").item(0).getTextContent()));
            chars.setGenre(element.getElementsByTagName("genre").item(0).getTextContent());
            chars.setReaders(element.getElementsByTagName("readers").item(0).getTextContent());
            chars.setIndex(Integer.parseInt(element.getElementsByTagName("index").item(0).getTextContent()));
        }
        return chars;
    }

}
