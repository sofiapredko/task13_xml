package com.sofia;

import com.sofia.filechecker.ExtensionChecker;
import com.sofia.model.Book;
import com.sofia.parsers.dom.DOMParserUser;
import com.sofia.parsers.sax.SAXParserUser;
import com.sofia.parsers.stax.StAXReader;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class Parser {
    public static void main(String[] args) {
        File xml = new File("src\\main\\resources\\xml\\booksXML.xml");
        File xsd = new File("src\\main\\resources\\xml\\booksXSD.xsd");

        if (checkIfXML(xml) && checkIfXSD(xsd)) {
            //printList(SAXParserUser.parseBeers(xml, xsd), "SAX");
            printList(StAXReader.parseBooks(xml, xsd), "StAX");
            //printList(DOMParserUser.getBookList(xml, xsd), "DOM");
        }
    }

    private static boolean checkIfXSD(File xsd) {
        return ExtensionChecker.isXSD(xsd);
    }

    private static boolean checkIfXML(File xml) {
        return ExtensionChecker.isXML(xml);
    }

    private static void printList(List<Book> books, String parserName) {
        Collections.sort(books, new BookComparator());
        System.out.println(parserName);
        for (Book book : books) {
            System.out.println(book);
        }
    }
}
