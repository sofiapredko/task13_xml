package com.sofia.model;

public class Chars {
    private int pages;
    private boolean colourful;
    private String genre;
    private String readers;
    private int index;

    public Chars() {
    }

    public Chars(int pages, boolean colourful, String genre, String readers, int index) {
        this.pages = pages;
        this.colourful = colourful;
        this.genre = genre;
        this.readers = readers;
        this.index = index;
    }

    public int getPages() {
        return pages;
    }

    public int getIndex() {
        return index;
    }

    public String getGenre() {
        return genre;
    }

    public String getReaders() {
        return readers;
    }

    public boolean isColourful() {
        return colourful;
    }

    public void setColourful(boolean colourful) {
        this.colourful = colourful;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setReaders(String readers) {
        this.readers = readers;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    @Override
    public String toString() {
        return "Chars{" +
                ", pages='" + pages + '\'' +
                ", colourful=" + colourful +
                ", genre='" + genre + '\'' +
                ", readers=" + readers +
                ", index=" + index +
                '}';
    }

}
