package com.sofia.model;

public class Book {
    private String title;
    private String author;
    private String type;
    private Chars chars;

    public Book() {
    }

    public Book(String title, String author, String type, Chars chars) {
        this.title = title;
        this.author = author;
        this.type = type;
        this.chars = chars;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getType() {
        return type;
    }

    public Chars getChars() {
        return chars;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setChars(Chars chars) {
        this.chars = chars;
    }

    @Override
    public String toString() {
        return "Book{" +
                " title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", type='" + type + '\'' +
                ", chars=" + chars +
                '}';
    }

}
